package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.service.AddressService;

@RestController
@RequestMapping("/api/address")
public class AddressController {

    @Autowired
    AddressService addressService;

    /**
     * Method that gets all addresses
     *
     * @return ApiResponse
     */
    @GetMapping
    public HttpEntity<?> getAll() {
        ApiResponse apiResponse = addressService.getAll();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }



    /**
     * Method that gets address by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = addressService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }



    /**
     * Method which add new Address
     *
     * @param address Address
     * @return ApiResponse
     */
    @PostMapping
    public HttpEntity<?> add(@RequestBody Address address) {
        ApiResponse apiResponse = addressService.add(address);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }



    /**
     * Method which edits the address by its ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @PutMapping("/{id}")
    public HttpEntity<?> update(@PathVariable Long id, @RequestBody Address updatingAddress) {
        ApiResponse apiResponse = addressService.update(id, updatingAddress);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 404).body(apiResponse);
    }


    /**
     * Method which deletes address by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = addressService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
