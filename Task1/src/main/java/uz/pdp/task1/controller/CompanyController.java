package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CompanyDto;
import uz.pdp.task1.service.AddressService;
import uz.pdp.task1.service.CompanyService;

@RestController
@RequestMapping("/api/company")
public class CompanyController {


    @Autowired
    CompanyService companyService;


    /**
     * Method that gets all companies
     *
     * @return ApiResponse
     */
    @GetMapping
    public HttpEntity<?> getAll() {
        ApiResponse apiResponse = companyService.getAll();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }



    /**
     * Method that gets company by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = companyService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }


    /**
     *  Method which adds new Company and edits
     * @param dto CompanyDto
     * @return ApiResponse
     */
    @PostMapping
    public HttpEntity<?> addOrUpdate(@RequestBody CompanyDto dto) {
        ApiResponse apiResponse = companyService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved") ? 201
                : apiResponse.getMessage().equals("Updated") ? 202
                : 409).body(apiResponse);
    }


    /**
     * Method which deletes company by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = companyService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
