package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CompanyDto;
import uz.pdp.task1.payload.DepartmentDto;
import uz.pdp.task1.service.DepartmentService;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {


    @Autowired
    DepartmentService departmentService;


    /**
     * Method that gets all Departments
     *
     * @return ApiResponse
     */
    @GetMapping
    public HttpEntity<?> getAll() {
        ApiResponse apiResponse = departmentService.getAll();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    /**
     * Method that gets Department by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = departmentService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }


    /**
     * Method which adds new Department and edits
     *
     * @param dto DepartmentDto
     * @return ApiResponse
     */
    @PostMapping
    public HttpEntity<?> addOrUpdate(@RequestBody DepartmentDto dto) {
        ApiResponse apiResponse = departmentService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved") ? 201
                : apiResponse.getMessage().equals("Updated") ? 202
                : 409).body(apiResponse);
    }


    /**
     * Method which deletes Department by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = departmentService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
