package uz.pdp.task1.payload;

import lombok.Data;

@Data
public class DepartmentDto {

    private Long id;
    private String name;
    private Long companyId;


}
