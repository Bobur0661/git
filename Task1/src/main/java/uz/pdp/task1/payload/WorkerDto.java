package uz.pdp.task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkerDto {


    private Long id;

    @NotNull(message = "It must not be blank. Please enter your name.")
    private String name;

    @NotNull(message = "It must not be blank. Please enter your phone number.")
    private String phoneNumber;

    @NotNull(message = "It must not be blank. Please enter Department Id.")
    private Long departmentId;

    @NotNull(message = "It must not be blank. Please enter Address Id.")
    private Long addressId;

}
