package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.repository.AddressRepo;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {


    @Autowired
    AddressRepo addressRepo;


    /**
     * The method that gets all addresses
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        List<Address> addresses = addressRepo.findAll();
        return addresses != null ? new ApiResponse(true, "Success", addresses)
                : new ApiResponse(false, "Failed");
    }


    /**
     * Method that gets address by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Address> optionalAddress = addressRepo.findById(id);
//        if (optionalAddress.isPresent()) {
//            return new ApiResponse(true, "Success", optionalAddress.get())
//        }
//        return new ApiResponse(false, "Not Found"); shu code pasdagi code ga teng!
        return optionalAddress.map(address -> new ApiResponse(true, "Success", address))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which add new Address
     *
     * @param address Address
     * @return ApiResponse
     */
    public ApiResponse add(Address address) {
        boolean existsByHomeNumberAndStreet = addressRepo.existsByHomeNumberAndStreet(address.getHomeNumber(), address.getStreet());
        if (existsByHomeNumberAndStreet) {
            return new ApiResponse(false, "This address already exists");
        }
        addressRepo.save(address);
        return new ApiResponse(true, "Saved");
    }


    /**
     * Method which edits the address by its ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse update(Long id, Address updatingAddress) {
        Optional<Address> optionalAddress = addressRepo.findById(id);
        if (!optionalAddress.isPresent()) {
            return new ApiResponse(false, "Not Found");
        }
        Address address = optionalAddress.get();
        address.setStreet(updatingAddress.getStreet());
        address.setHomeNumber(updatingAddress.getHomeNumber());
        addressRepo.save(address);
        return new ApiResponse(true, "Updated!", address);
    }


    /**
     * Method which deletes address by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            addressRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }
}
