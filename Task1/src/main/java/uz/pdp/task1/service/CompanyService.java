package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.entity.Company;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CompanyDto;
import uz.pdp.task1.repository.AddressRepo;
import uz.pdp.task1.repository.CompanyRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {


    @Autowired
    CompanyRepo companyRepo;

    @Autowired
    AddressRepo addressRepo;


    /**
     * Method that gets all companies
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        List<Company> companyList = companyRepo.findAll();
        return companyList != null ? new ApiResponse(true, "Success", companyList)
                : new ApiResponse(false, "Failed");
    }


    /**
     * Method that gets company by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Company> optionalCompany = companyRepo.findById(id);
//        if (!optionalCompany.isPresent()) {
//            return new ApiResponse(false, "Not Found");
//        }
//        return new ApiResponse(true, "Success", optionalCompany.get());
        return optionalCompany.map(company -> new ApiResponse(true, "Success", company))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which adds new Company and edits
     *
     * @param dto CompanyDto
     * @return ApiResponse
     */
    public ApiResponse addOrUpdate(CompanyDto dto) {
        Company company = new Company();
        if (dto.getId() != null) {
            company = companyRepo.getById(dto.getId());
        }
        Optional<Address> optionalAddress = addressRepo.findById(dto.getAddressId());
        if (!optionalAddress.isPresent()) {
            return new ApiResponse(false, "Address not Found!");
        }
        company.setCorpName(dto.getCorpName());
        company.setDirectorName(dto.getDirectorName());
        company.setAddress(optionalAddress.get());
        companyRepo.save(company);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }


    /**
     * Method which deletes company by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            companyRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }
}
