package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.entity.Company;
import uz.pdp.task1.entity.Department;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CompanyDto;
import uz.pdp.task1.payload.DepartmentDto;
import uz.pdp.task1.repository.AddressRepo;
import uz.pdp.task1.repository.CompanyRepo;
import uz.pdp.task1.repository.DepartmentRepo;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {


    @Autowired
    DepartmentRepo departmentRepo;

    @Autowired
    CompanyRepo companyRepo;


    /**
     * Method that gets all Departments
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        List<Department> departments = departmentRepo.findAll();
        return departments != null ? new ApiResponse(true, "Success", departments)
                : new ApiResponse(false, "Failed");
    }


    /**
     * Method that gets department by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Department> optionalDepartment = departmentRepo.findById(id);
//        if (!optionalCompany.isPresent()) {
//            return new ApiResponse(false, "Not Found");
//        }
//        return new ApiResponse(true, "Success", optionalCompany.get());
        return optionalDepartment.map(department -> new ApiResponse(true, "Success", department))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which adds new Department and edits
     *
     * @param dto DepartmentDto
     * @return ApiResponse
     */
    public ApiResponse addOrUpdate(DepartmentDto dto) {
        try {
            Department department = new Department();
            if (dto.getId() != null) {
                department = departmentRepo.getById(dto.getId());
            }
            Optional<Company> companyOptional = companyRepo.findById(dto.getCompanyId());
            if (!companyOptional.isPresent()) {
                return new ApiResponse(false, "Company not Found!");
            }
            department.setName(dto.getName());
            department.setCompany(companyOptional.get());
            departmentRepo.save(department);
            return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
        } catch (Exception e) {
            return new ApiResponse(false, "There is the category with the same name");
        }
    }


    /**
     * Method which deletes department by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            departmentRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }
}
