package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.entity.Department;
import uz.pdp.task1.entity.Worker;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.WorkerDto;
import uz.pdp.task1.repository.AddressRepo;
import uz.pdp.task1.repository.DepartmentRepo;
import uz.pdp.task1.repository.WorkerRepo;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerService {

    @Autowired
    WorkerRepo workerRepo;

    @Autowired
    DepartmentRepo departmentRepo;

    @Autowired
    AddressRepo addressRepo;


    /**
     * Get All Workers
     *
     * @return Workers
     */
    public ApiResponse getAllWorkers() {
        List<Worker> workers = workerRepo.findAll();
        return workers != null ? new ApiResponse(true, "Success", workers) : new ApiResponse(false, "Failed");
    }


    /**
     * Get One Worker By ID
     *
     * @param id Long
     * @return ApiResponse
     * If there is no worker with entered id, the method returns null.
     */
    public ApiResponse getById(Long id) {
        Optional<Worker> workerOptional = workerRepo.findById(id);
//        if (!workerOptional.isPresent()) {
//            return new ApiResponse(false, "Worker not Found!");
//        }
//        return new ApiResponse(true, "Success", workerOptional.get());
        return workerOptional.map(worker -> new ApiResponse(true, "Success", worker)).orElseGet(() -> new ApiResponse(false, "Worker not Found!"));
    }


    /**
     * Delete Worker by Worker ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        Optional<Worker> optionalWorker = workerRepo.findById(id);
        if (!optionalWorker.isPresent()) {
            return new ApiResponse(false, "Worker Not Found");
        }
        workerRepo.deleteById(optionalWorker.get().getId());
        return new ApiResponse(true, "Deleted!");
    }


    /**
     * Method which adds new worker!
     *
     * @param dto WorkerDto
     * @return ApiResponse
     */
    public ApiResponse add(WorkerDto dto) {
        Worker worker = new Worker();
        boolean existsByPhoneNumber = workerRepo.existsByPhoneNumber(dto.getPhoneNumber());
        if (existsByPhoneNumber) {
            return new ApiResponse(false, "This Phone Number already exists.");
        }

        Optional<Department> optionalDepartment = departmentRepo.findById(dto.getDepartmentId());
        if (!optionalDepartment.isPresent()) {
            return new ApiResponse(false, "Department not found!");
        }

        Optional<Address> optionalAddress = addressRepo.findById(dto.getAddressId());
        if (!optionalAddress.isPresent()) {
            return new ApiResponse(false, "Address not fiund!");
        }

        worker.setName(dto.getName());
        worker.setPhoneNumber(dto.getPhoneNumber());
        worker.setDepartment(optionalDepartment.get());
        worker.setAddress(optionalAddress.get());
        workerRepo.save(worker);
        return new ApiResponse(true, "Saved");
    }


    /**
     * Method That Updates Worker
     *
     * @param id  Long
     * @param dto WorkerDto
     * @return ApiResponse
     */
    public ApiResponse update(Long id, WorkerDto dto) {
        Optional<Worker> optionalWorker = workerRepo.findById(id);
        if (!optionalWorker.isPresent()) {
            return new ApiResponse(false, "Worker Not Found!");
        }
        boolean existsByPhoneNumberAndIdNot = workerRepo.existsByPhoneNumberAndIdNot(dto.getPhoneNumber(), id);
        if (existsByPhoneNumberAndIdNot) {
            return new ApiResponse(false, "This phone number already exists");
        }

        Optional<Department> optionalDepartment = departmentRepo.findById(dto.getDepartmentId());
        if (!optionalDepartment.isPresent()) {
            return new ApiResponse(false, "Department not found!");
        }

        Optional<Address> optionalAddress = addressRepo.findById(dto.getAddressId());
        if (!optionalAddress.isPresent()) {
            return new ApiResponse(false, "Address not fiund!");
        }
        Worker worker = optionalWorker.get();
        worker.setName(dto.getName());
        worker.setPhoneNumber(dto.getPhoneNumber());
        worker.setDepartment(optionalDepartment.get());
        worker.setAddress(optionalAddress.get());
        workerRepo.save(worker);
        return new ApiResponse(true, "Updated");
    }
}
